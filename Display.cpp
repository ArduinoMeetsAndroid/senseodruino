/*
 // * SenseoMenu.cpp
 *
 *  Created on: 20.01.2017
 *      Author: christianrichter
 */

#include "Display.h"

//#include <string.h>
senseo::electronic::Display::Display(LiquidCrystal *lcd,
		senseo::logger::Logger *logger) {
	lcd_ = lcd;
//	_senseoDruino = senseoDruino;
	lcd_->begin(MAXCOLS_, MAXROWS_);
	logger_ = logger;
}

void senseo::electronic::Display::lcdClearDisplay() {
	lcd_->clear();
}

void senseo::electronic::Display::lcdSetCursor(int col, int row) {
	lcd_->setCursor(col, row);
}

void senseo::electronic::Display::lcdRemoveRow(int row) {
	lcd_->setCursor(0, row);
	for (int i = 0; i < MAXCOLS_; i++) {
		lcd_->setCursor(i, row);
		lcd_->write(" ");
	}
}

void senseo::electronic::Display::lcdWriteLine(String msg, int col, int row,
		bool deleteRow) {
	if (deleteRow) {
		lcd_->setCursor(col, row);
		for (int i = 0; i < MAXCOLS_; i++) {
			lcd_->setCursor(i, row);
			lcd_->write(" ");
		}
	}
	lcd_->setCursor(col, row);
	lcd_->print(msg);
}

void senseo::electronic::Display::lcdWriteLine(int msg, int col, int row,
		bool deleteRow) {
	if (deleteRow) {
		lcd_->setCursor(col, row);
		for (int i = 0; i < MAXCOLS_; i++) {
			lcd_->setCursor(i, row);
			lcd_->write(" ");
		}
	}
	lcd_->setCursor(col, row);
	lcd_->print(msg);
}

void senseo::electronic::Display::lcdWrite(int numberOfImg) {
	lcd_->write(numberOfImg);
}

void senseo::electronic::Display::lcdWriteImage(int col, int row,
		bool deleteRow) {
//	_lcd->createChar(7, cupimage);

	if (deleteRow) {
		lcd_->setCursor(col, row);
		for (int i = 0; i < MAXCOLS_; i++) {
			lcd_->setCursor(i, row);
			lcd_->write(" ");
		}
	}
	lcd_->setCursor(col, row);
	for (int i = 0; i < 10; i++) {
		lcdSetCursor(0, 0);
		lcd_->write(6);
		delay(500);
		lcdSetCursor(0, 0);
		lcd_->write(7);
		delay(500);
	}
}

LiquidCrystal senseo::electronic::Display::getLcd() {
	return *lcd_;
}

int senseo::electronic::Display::getMaxLcdRows() {
	return MAXROWS_;
}

int senseo::electronic::Display::getMaxLcdCols() {
	return MAXCOLS_;
}

