/*
 Strings.h

 Created on: 21.07.2017
 Author: christianrichter
 */

#ifndef LIBS_UTILS_STRINGS_H_
#define LIBS_UTILS_STRINGS_H_
//#include <string.h>
#include <WString.h>

namespace senseo {
namespace ui {
namespace strings {

class Strings {

private:
	Strings();
};
static String ON = "AN ";
static String OFF = "AUS";
static String DONE = "Fertig";
static String WATER_EMPTY = "Wasser nachf\365llen";
static String WATER_HEATING = "wird erw\341rmt . .";
static String WATER_IS_HOT = "Wasser ist hei\342";
static String ARROW_LEFT = "<";
static String ARROW_RIGHT = ">";
static String AUTO_MODE = "Auto-Mode";
static String TEMPERATION = "Temperatur";
static String FILLING = "     F\365llen";

} /* namespace strings */
} /* namespace ui */
} /* namespace senseo */

#endif
