/*
 * Logger.h
 *
 *  Created on: 21.07.2017
 *      Author: christianrichter
 */

#ifndef LIBS_LOGGER_LOGGER_H_
#define LIBS_LOGGER_LOGGER_H_

#include "Arduino.h"
#include <string.h>

namespace senseo {
namespace logger {

class Logger {
public:
	Logger(int baudRate);

	void setPrintDebugInfos(bool showDebbug);
	void setBluetoothCmd(bool showBt);

	void debugInfo(String info);
	void debugInfo(int info);
	void debugError(String info);
	void bluetooth(String cmd);

private:

	bool showDebug_ = false;	// is debug mode on?
	bool showBt_ = true;		// is bluetooth mode on?
};

} /* namespace logger */
} /* namespace senseo */

#endif /* LIBS_LOGGER_LOGGER_H_ */
