/*
 * Logger.cpp
 *
 *  Created on: 21.07.2017
 *      Author: christianrichter
 */

#include "Logger.h"

namespace senseo {
namespace logger {

Logger::Logger(int baudRate) {
	Serial.begin(baudRate);
	showBt_ = true;
	showDebug_ = false;
}

void Logger::setPrintDebugInfos(bool showDebug = false) {
	showDebug_ = showDebug;
}
void Logger::setBluetoothCmd(bool showBt = true) {
	showBt_ = showBt;
}

void Logger::debugInfo(String info) {
	if (showDebug_) {
		Serial.print("DEBUG -> ");
		Serial.println(info);
	}
}

void Logger::debugInfo(int info) {
	if (showDebug_) {
		Serial.print("DEBUG -> ");
		Serial.println(info);
	}
}

void Logger::debugError(String info) {
	if (showDebug_) {
		Serial.print("ERROR -> ");
		Serial.println(info);
	}
}

void Logger::bluetooth(String cmd) {
	if (showBt_) {
		Serial.println(cmd);
		Serial.println('\n');
	}
}

} /* namespace logger */
} /* namespace senseo */
