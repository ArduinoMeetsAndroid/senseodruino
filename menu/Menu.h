/*
 * Menu.h
 *
 *  Created on: 19.07.2017
 *      Author: christianrichter
 */

#ifndef MENU_MENU_H_
#define MENU_MENU_H_
#include "../Display.h"

namespace senseo {
namespace ui {
class Menu {
public:
	Menu(senseo::electronic::Display *display, logger::Logger *logger);
	virtual ~Menu();

	// Menu Navigation
	void printMenue();
	void moveMenuCursorDown();
	void moveMenuCursorUp();
	void selectMenuEntry();

	// Menu Options
	void menuOptionSetAutoMode();
	void menuOptionHeatWater();
	void menuOptionLcdBacklight();
	void menuOptionHardwareTest();

	int getSelectedCupSize();
	int getMenuEntrieSize();
	String getSelectedMainMenuItem();
	int getSelectedMainMenuPosition();

	String getSelectedSubMenuItem();
	int getSelectedSubMenuPosition();

	String getNextCupSizeItem();
	String getPreviousCupSizeItem();
	String getSubMenuItemByPosition(int position);

	void setActiveMenu(int activeMenu);

	void setAutoMode(bool autoMode);
	void setMenuMode(bool menuMode);bool isAutoMode();bool isMenuMode();
	void createChars();

	static int const MAIN_MENU_ = 0;
	static int const CUP_SIZES_MENU_ = 1;

private:
	senseo::electronic::Display *_display;
	logger::Logger *logger_;

	int cursorLine_ = 0;
	int displayFirstLine_ = 0;

	bool menuActive_ = false;bool autoMode_ = false;

	int selectedMenuItem_ = 0;
	int selectedSubMenuItem_ = 2;
	int activeMenu_ = MAIN_MENU_;

	String MAIN_MENU_COLS_[6] = {   "  1. Temperatur",
									"  2. Tassengr\357\342e",
									"  3. Auto-Mode",
									"  4. Display",
									"  5. Boiler Füllen",
									"  6. Hardware Test" };

	String CUP_SIZES_COLS_[4] = {   "      Espresso",
									"       Klein",
									"       Normal",
									"       Gro\342" };
	int CUP_SIZES_VALUES_[4] = {25, 45, 85, 100};

};
}
}

#endif /* MENU_MENU_H_ */
