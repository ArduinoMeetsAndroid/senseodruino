/*
 * Menu.cpp
 *
 *  Created on: 19.07.2017
 *      Author: christianrichter
 */

#include "Menu.h"
//#include "../images/Images.h"

senseo::ui::Menu::Menu(senseo::electronic::Display *display,
		logger::Logger *logger) {
	_display = display;
	logger_ = logger;

}

senseo::ui::Menu::~Menu() {
	// TODO Auto-generated destructor stub
}

void senseo::ui::Menu::createChars() {
	/*
	 _display->getLcd().createChar(0, cupDownLeft);
	 _display->getLcd().createChar(6, cupDownRight);
	 _display->getLcd().createChar(6, cloudLeft);
	 _display->getLcd().createChar(6, cloudRight);
	 _display->getLcd().createChar(6, cloudLeftNegative);
	 _display->getLcd().createChar(6, cloudRightNegative);
	 _display->getLcd().createChar(6, loadingOneLeft);
	 _display->getLcd().createChar(6, loadingOneRight);
	 _display->getLcd().createChar(6, loadingTwoLeft);
	 _display->getLcd().createChar(6, loadingTwoRight);
	 _display->getLcd().createChar(6, loadingTreeLeft);
	 _display->getLcd().createChar(6, loadingTreeRight);
	 _display->getLcd().createChar(6, loadingFourLeft);
	 _display->getLcd().createChar(6, loadingFourRight);
	 _display->getLcd().createChar(6, loadingFiveLeft);
	 _display->getLcd().createChar(6, loadingSixLeft);
	 _display->getLcd().createChar(6, loadingSixRight);
	 */
}

void senseo::ui::Menu::printMenue() {
	menuActive_ = true;
	_display->lcdClearDisplay();

	for (int i = 0; i < _display->getMaxLcdRows(); i++) {
		_display->lcdWriteLine(MAIN_MENU_COLS_[displayFirstLine_ + i], 0, i,
		true);
		_display->lcdWriteLine(">", 0, (cursorLine_ - displayFirstLine_),
		false);
	}
}

String senseo::ui::Menu::getSubMenuItemByPosition(int position) {
	if (position < getMenuEntrieSize()) {
		return CUP_SIZES_COLS_[position];
	} else {
		return "";
	}
}

int senseo::ui::Menu::getSelectedMainMenuPosition() {
	return cursorLine_;
}

String senseo::ui::Menu::getSelectedSubMenuItem() {
	return CUP_SIZES_COLS_[selectedSubMenuItem_];
}

int senseo::ui::Menu::getSelectedSubMenuPosition() {
	return selectedSubMenuItem_;
}

String senseo::ui::Menu::getSelectedMainMenuItem() {
	return MAIN_MENU_COLS_[selectedSubMenuItem_];
}
void senseo::ui::Menu::setMenuMode(bool menuActive) {
	menuActive_ = menuActive;
}

bool senseo::ui::Menu::isMenuMode() {
	return menuActive_;
}

int senseo::ui::Menu::getMenuEntrieSize() {
	if (activeMenu_ == MAIN_MENU_) {
		return (sizeof(MAIN_MENU_COLS_) / sizeof(*MAIN_MENU_COLS_));
	} else if (activeMenu_ == CUP_SIZES_MENU_) {
		return (sizeof(CUP_SIZES_COLS_) / sizeof(*CUP_SIZES_COLS_));
	}
}

void senseo::ui::Menu::moveMenuCursorDown() {
	if (cursorLine_ == (displayFirstLine_ + _display->getMaxLcdRows() - 1)) {
		displayFirstLine_++;
	}
	if (cursorLine_ == (getMenuEntrieSize() - 1)) {
		cursorLine_ = 0;
		displayFirstLine_ = 0;
	} else {
		cursorLine_ = cursorLine_ + 1;
	}
	printMenue();
}

void senseo::ui::Menu::moveMenuCursorUp() {
	if ((displayFirstLine_ == 0) & (cursorLine_ == 0)) {
		displayFirstLine_ = getMenuEntrieSize() - _display->getMaxLcdRows();
	} else if (displayFirstLine_ == cursorLine_) {
		displayFirstLine_--;
	}
	if (cursorLine_ == 0) {
		cursorLine_ = getMenuEntrieSize() - 1;
	} else {
		cursorLine_ = cursorLine_ - 1;
	}
	printMenue();
}

String senseo::ui::Menu::getNextCupSizeItem() {
	String tmpString;
	logger_->debugInfo("getNextCupSizeItem()");
	logger_->debugInfo(getSelectedSubMenuPosition());
	logger_->debugInfo(getMenuEntrieSize());
	if (activeMenu_ == CUP_SIZES_MENU_) {
		if (getSelectedSubMenuPosition() < getMenuEntrieSize()) {
			tmpString = getSubMenuItemByPosition(++selectedSubMenuItem_);
			logger_->debugInfo(tmpString);
			if (selectedSubMenuItem_ >= getMenuEntrieSize()) {
				selectedSubMenuItem_ -= 1;
				tmpString = getSubMenuItemByPosition(selectedSubMenuItem_);
			}
		}
	}
	return tmpString;
}

int senseo::ui::Menu::getSelectedCupSize() {
	return CUP_SIZES_VALUES_[selectedSubMenuItem_];
}

String senseo::ui::Menu::getPreviousCupSizeItem() {
	String tmpString;
	logger_->debugInfo("getPreviousCupSizeItem()");
	logger_->debugInfo(activeMenu_);
	if (activeMenu_ == CUP_SIZES_MENU_) {
		if (getSelectedSubMenuPosition() > 0) {
			tmpString = getSubMenuItemByPosition(--selectedSubMenuItem_);
			logger_->debugInfo(tmpString);
		} else if (selectedSubMenuItem_ < 0) {
			selectedSubMenuItem_ = 0;
		}
		tmpString = getSubMenuItemByPosition(selectedSubMenuItem_);
	}
	return tmpString;
}

void senseo::ui::Menu::setAutoMode(bool autoMode) {
	autoMode_ = autoMode;
}

bool senseo::ui::Menu::isAutoMode() {
	return autoMode_;
}

void senseo::ui::Menu::setActiveMenu(int activeMenu) {
	activeMenu_ = activeMenu;
}

