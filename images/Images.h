/*
 * images.h
 *
 *  Created on: 19.07.2017
 *      Author: christianrichter
 */

#ifndef IMAGES_IMAGES_H_
#define IMAGES_IMAGES_H_

#include "Arduino.h"

#define byte uint8_t

class Images {
public:
	Images();
	void printCup();



byte cup1[8] = { 0b00101,
			     0b01010,
				 0b00000,
				 0b10010,
				 0b10011,
				 0b10011,
				 0b10010,
				 0b11110 };

byte cup2[8] = { 0b10100,
				 0b01010,
				 0b00000,
				 0b10010,
				 0b10011,
				 0b10011,
				 0b10010,
				 0b11110 };

byte cupDownLeft[8] = {
				0b11111,
				0b10000,
				0b10000,
				0b10000,
				0b10000,
				0b10000,
				0b10000,
				0b01111
			};

byte cupDownRight[8] = {
				0b11110,
				0b00101,
				0b00101,
				0b00101,
				0b00101,
				0b00110,
				0b00100,
				0b11000
			};

byte cloudLeft[8] = {
				0b01010,
				0b01010,
				0b10010,
				0b01001,
				0b01010,
				0b10010,
				0b10001,
				0b00000
			};

byte cloudRight[8] = {
				0b01000,
				0b00100,
				0b01000,
				0b01000,
				0b00100,
				0b00100,
				0b01000,
				0b00000
			};

byte cloudLeftNegative[8] = {
				0b10001,
				0b10010,
				0b01001,
				0b10001,
				0b10010,
				0b01010,
				0b01001,
				0b00000
			};

byte cloudRightNegative[8] = {
				0b01000,
				0b01000,
				0b00100,
				0b01000,
				0b01000,
				0b00100,
				0b00100,
				0b00000
			};

byte loadingOneLeft[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b01111,
				0b00000
			};

byte loadingOneRight[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b11110,
				0b00000
			};

byte loadingTwoRight[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b01111,
				0b01111,
				0b00000
			};

byte loadingTwoLeft[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b11110,
				0b11110,
				0b00000
			};

byte loadingTreeRight[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b01111,
				0b01111,
				0b01111,
				0b00000
			};

byte loadingTreeLeft[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b11110,
				0b11110,
				0b11110,
				0b00000
			};

byte loadingFourRight[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b01111,
				0b01111,
				0b01111,
				0b01111,
				0b00000
			};

byte loadingFourLeft[8] = {
				0b00000,
				0b00000,
				0b00000,
				0b11110,
				0b11110,
				0b11110,
				0b11110,
				0b00000
			};

byte loadingFiveRight[8] = {
				0b00000,
				0b00000,
				0b01111,
				0b01111,
				0b01111,
				0b01111,
				0b01111,
				0b00000
			};

byte loadingFiveLeft[8] = {
				0b00000,
				0b00000,
				0b11110,
				0b11110,
				0b11110,
				0b11110,
				0b11110,
				0b00000
			};

byte loadingSixRight[8] = {
				0b00000,
				0b01111,
				0b01111,
				0b01111,
				0b01111,
				0b01111,
				0b01111,
				0b00000
			};

byte loadingSixLeft[8] = {
				0b00000,
				0b11110,
				0b11110,
				0b11110,
				0b11110,
				0b11110,
				0b11110,
				0b00000
			};

};
#endif /* IMAGES_IMAGES_H_ */
