/*
 * SenseoMenu.h
 *
 *  Created on: 20.01.2017
 *      Author: christianrichter
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <WString.h>
#include <Arduino.h>
#include <LiquidCrystal.h>
#include <string.h>
#include "libs/logger/Logger.h"

namespace senseo {
namespace electronic {
class Display {
public:
	Display(LiquidCrystal *lcd, senseo::logger::Logger *logger);
	void printSenseoVersion(bool autoMode);

	// Lcd write
	void lcdClearDisplay();
	void lcdRemoveRow(int row);
	void lcdSetCursor(int col, int row);
	void lcdWriteLine(String msg, int col, int row, bool deleteRow);
	void lcdWriteLine(int msg, int col, int row, bool deleteRow);
	void lcdWriteImage(int col, int row, bool deleteRow);
	void lcdWrite(int numberOfImg);

	LiquidCrystal getLcd();

	int getMaxLcdRows();
	int getMaxLcdCols();

private:
	LiquidCrystal *lcd_;
	senseo::logger::Logger *logger_;	// local instance for logging
	int const MAXROWS_ = 4;				// set the rows of the display
	int const MAXCOLS_ = 20;			// the the collums of the display
};
}
}
#endif /* DISPLAY_H_ */
