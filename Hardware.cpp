/*
 * Hardware.cpp
 *
 *  Created on: 20.01.2017
 *      Author: christianrichter
 */

#include "Hardware.h"

senseo::electronic::Hardware::Hardware(senseo::ui::Menu *menu,
		senseo::electronic::Display *display, logger::Logger *logger) {
	menu_ = menu;
	display_ = display;
	logger_ = logger;

	// Hall Sensor
	pinMode(HALLSENSOR_, INPUT);

	// Boiler
	pinMode(BOILER_, OUTPUT);
	digitalWrite(BOILER_, LOW);

	// LED
	pinMode(GREENLED_, OUTPUT);
	pinMode(BLUELED_, OUTPUT);

	// Pump
	pinMode(PUMP_, OUTPUT);
	digitalWrite(PUMP_, LOW);

	//Display
	pinMode(LEDBACKLIGHT_, OUTPUT);
	//digitalWrite(_LEDBACKLIGHT, HIGH);

	//Buttons
	pinMode(BUTTONLEFT_, INPUT_PULLUP);
	pinMode(BUTTONCENTER_, INPUT_PULLUP);
	pinMode(BUTTONRIGHT_, INPUT_PULLUP);
}

void senseo::electronic::Hardware::noWaterLed() {
	while (getHallSensor() == HIGH) {
		enableBlueLed();
		delay(100);
		disableBlueLed();
		enableGreenLed();
		delay(100);
		disableGreenLed();
	}
}

void senseo::electronic::Hardware::setRunning(bool running) {
	running_ = running;
	display_->lcdWriteLine(running_ + "", 0, 3, true);
}

void senseo::electronic::Hardware::heatBoiler() {
	display_->lcdWriteLine(senseo::ui::strings::WATER_HEATING, 2, 1, true);
	logger_->bluetooth("INFO&Heating");
	logger_->debugInfo("HeatBoiler");
	running_ = true;
	String inputString;

	int temp = 0;
	while (temp <= temperationValue_ && running_) {
		display_->lcdSetCursor(0, 0);
		display_->lcdWrite(6);
		logger_->debugInfo(running_);

		temp = getBoilerSensorValue();
		logger_->debugInfo(temp);
		if (!isWaterEmpty()) {
			enableBoiler();
		} else if (isWaterEmpty()) {
			disableBoiler();
			noWaterLed();
			while (isWaterEmpty()) {
			}
		}

		delay(100);
		if (isButtonCenterPressed()) {
			disableBoiler();
			disablePump();
			running_ = false;
			break;
		}

		char inChar = (char) Serial.read();
		inputString += inChar;
		if (inChar == '\n' || inChar == '\r') {
			stringComplete = true;
		}
		if (inputString.equals("POWEROFF")) {
			display_->lcdWriteLine(senseo::ui::strings::OFF, 3, 3, true);
			running_ = false;

		}

		display_->lcdSetCursor(0, 0);
		display_->lcdWrite(7);

	}

	disableBoiler();
	setRunning(false);
	logger_->bluetooth("INFO&Done");

//	logger_->bluetooth("DONE&DUMMY");

	if (menu_->isAutoMode()) {
		fillCup(45);
	} else {
		logger_->bluetooth("DONE&ENABLE_CUPS");
	}

// Water is now Hot
	printSoftwareVersion();
}

void senseo::electronic::Hardware::fillCup(int fillTime) {
	display_->lcdWriteLine(senseo::ui::strings::FILLING, 0, 1, true);
	int currentTime = 0;
	unsigned long previousMillis = 0;    // will store last time LED was updated
	const long interval = 1000;
	bool running = true;


	while (currentTime <= fillTime && running) {
		unsigned long currentMillis = millis();

		if (currentMillis - previousMillis >= interval) {
			previousMillis = currentMillis;
			currentTime++;
		}
		if (isWaterEmpty()) {
			disableBoiler();
			disablePump();
			noWaterLed();
			break;
		}

		enablePump();

		logger_->debugInfo(currentTime);
		if (isButtonCenterPressed()) {
			disablePump();
			running = false;
			break;
		}
	}
	disablePump();
	display_->lcdWriteLine(senseo::ui::strings::DONE, 7, 3, true);
	delay(1000);
	printSoftwareVersion();
}

int senseo::electronic::Hardware::calculateProzentToHeat() {
	// currentTempValue = analogRead(BOILER_SENSOR);
	return (100 * analogRead(BOILERSENSOR_)) / MAXTEMP_;
}

bool senseo::electronic::Hardware::isButtonLeftPressed() {
	btnLeft_->read();
	return btnLeft_->isPressed();
}

bool senseo::electronic::Hardware::isButtonCenterPressed() {
	/*
	btnCenter_->read();
	return btnCenter_->isPressed();
	*/
	return digitalRead(BUTTONCENTER_);
}

bool senseo::electronic::Hardware::isButtonCenterLongPressed() {
	btnCenter_->read();
	return btnCenter_->pressedFor(LONG_PRESS_TIME_);
}

bool senseo::electronic::Hardware::isButtonRightPressed() {
	btnRight_->read();
	return btnRight_->isPressed();
}

bool senseo::electronic::Hardware::isWaterEmpty() {
	if (getHallSensor() == HIGH) {
		if (!menu_->isMenuMode()) {
			display_->lcdWriteLine(senseo::ui::strings::WATER_EMPTY, 0, 3,
			true);
		}
		Serial.write("NO WATER\n");
		return true;
	} else {
		if (!menu_->isMenuMode()) {
			display_->lcdRemoveRow(3);
		}
		return false;
	}
}

bool senseo::electronic::Hardware::isWaterHot() {
	if (getBoilerSensor() >= temperationValue_) {
		disableBlueLed();
		enableGreenLed();
		if (!menu_->isMenuMode()) {
			display_->lcdWriteLine(senseo::ui::strings::WATER_IS_HOT, 2, 2,
			false);
		}
		return true;
	} else {
		disableGreenLed();
		enableBlueLed();
		if (!menu_->isMenuMode()) {
			display_->lcdRemoveRow(2);
		}
		return false;
	}
}

void senseo::electronic::Hardware::printSoftwareVersion() {
	display_->lcdClearDisplay();
	display_->lcdWriteLine("SenseoDruino", 4, 1, true);
	if (menu_->isAutoMode()) {
		display_->lcdWriteLine("Auto-Mode: ON", 3, 2, true);
	}
	display_->lcdWriteLine(menu_->getSelectedSubMenuItem(), 4, 3, true);
}

int senseo::electronic::Hardware::getTemperationValue() {
	return temperationValue_;
}

void senseo::electronic::Hardware::setTemperationValue(int temperationValue) {
	temperationValue_ = temperationValue;
}

void senseo::electronic::Hardware::setTemperationDown() {
	temperationValue_ -= 10;
}

void senseo::electronic::Hardware::setTemperationUp() {
	temperationValue_ += 10;
}

void senseo::electronic::Hardware::enableGreenLed() {
	digitalWrite(GREENLED_, LOW);
}

void senseo::electronic::Hardware::disableGreenLed() {
	digitalWrite(GREENLED_, HIGH);
}

void senseo::electronic::Hardware::enableBlueLed() {
	digitalWrite(BLUELED_, LOW);
}

void senseo::electronic::Hardware::disableBlueLed() {
	digitalWrite(GREENLED_, HIGH);
}

void senseo::electronic::Hardware::enableLedBacklight() {
	digitalWrite(LEDBACKLIGHT_, HIGH);
}

void senseo::electronic::Hardware::disableLedBacklight() {
	digitalWrite(LEDBACKLIGHT_, LOW);
}

bool senseo::electronic::Hardware::isDisplayOn() {
	return digitalRead(LEDBACKLIGHT_ == HIGH);
}

int senseo::electronic::Hardware::getHallSensor() {
	return digitalRead(HALLSENSOR_);
}

int senseo::electronic::Hardware::getBoilerSensorValue() {
	return analogRead(BOILERSENSOR_);
}

void senseo::electronic::Hardware::enableBoiler() {
//	if (!running_) {
	digitalWrite(BOILER_, HIGH);
//		running_ = true;
//	} else {
//		digitalWrite(BOILER_, LOW);
//		running_ = false;
//	}
}

void senseo::electronic::Hardware::disableBoiler() {
	digitalWrite(BOILER_, LOW);
}

void senseo::electronic::Hardware::enablePump() {
	digitalWrite(PUMP_, HIGH);
}

void senseo::electronic::Hardware::disablePump() {
	digitalWrite(PUMP_, LOW);
}

void senseo::electronic::Hardware::readButtons() {
	btnLeft_->read();
	btnRight_->read();
	btnCenter_->read();
}

