#ifndef SENSEODUINO_H_
#define SENSEODUINO_H_
//#include "Arduino.h"

#include "Display.h"
#include "Hardware.h"
#include "libs/logger/Logger.h"
#include <SoftwareSerial.h>

#define ROW_ONE 0
#define ROW_TWO 1
#define ROW_TREE 2
#define ROW_FOUR 3

class SenseoDruino {

public:
	void setAutoMode();
	void printSoftwareVersion();
	SenseoDruino getHandler();

	bool isPowerOnValue() const {
		return powerOnValue_;
	}

	void setPowerOnValue(bool isPowerOnValue = false) {
		powerOnValue_ = isPowerOnValue;
	}

	bool isBacklightValue() const {
		return backlightValue_;
	}

	void setBacklightValue(bool backlightValue = true) {
		this->backlightValue_ = backlightValue;
	}

	const String& getCommand() const {
		return command_;
	}

	void setCommand(const String& command = "") {
		this->command_ = command;
	}

	char getData() const {
		return data_;
	}

	void setData(char data = 0) {
		this->data_ = data;
	}

	int getDisplayFirstLine() const {
		return DisplayFirstLine_;
	}

	void setDisplayFirstLine(int displayFirstLine = 0) {
		DisplayFirstLine_ = displayFirstLine;
	}

	const String& getInputString() const {
		return inputString_;
	}
	bool running_ = false;			// set to true when the boiler is running

	void setInputString(const char& inputString) {
		this->inputString_ += inputString;
	}

	void clearInputString() {
		this->inputString_ = "";
	}

	bool isStringComplete() const {
		return stringComplete_;
	}

	void setStringComplete(bool stringComplete = false) {
		this->stringComplete_ = stringComplete;
	}

private:

	// Display
	bool backlightValue_ = true; 	// Backlight of the lcd is on
	int DisplayFirstLine_ = 0;		// the firtst line of the display

	char data_ = 0;					// incomming data
	bool stringComplete_ = false;// set to true when the incomming command done
	String inputString_ = "";		// the local value of command
	String command_ = "";			// the local value of command
	bool powerOnValue_ = false;		//
	bool tankEmpty_ = false;		// set to true if the tank is empty

};

const int BAUDRATE_ = 9600;			// Baudrate for

// Lcd init
LiquidCrystal *lcd = new LiquidCrystal(12, 11, 6, 5, 3, 2);

// Senseo Controller Class
SenseoDruino *senseoDruino = new SenseoDruino();

senseo::logger::Logger *logger_ = new senseo::logger::Logger(BAUDRATE_);

// Display Helper Utility
senseo::electronic::Display *display = new senseo::electronic::Display(lcd,
		logger_);

// Menu
senseo::ui::Menu *menu = new senseo::ui::Menu(display, logger_);

// Senseo Hardware Utility
senseo::electronic::Hardware *hardware = new senseo::electronic::Hardware(menu,
		display, logger_);

/**
 * Variables
 */

#endif /* SENSEODUINO_H_ */
