#include "SenseoDruino.h"
#include "Arduino.h"
#include <LiquidCrystal.h>

bool checkSetCups = true;
void mainMenuSelection();
void lcdBacklight();
// Temp values

// Prototypes
void sendData(String data);
bool isPowerOn();
void setCupsEnableOnAndroid();
void setCupsDisableOnAndroid();
void selectCupSize();
void temp();
void resetTimer();

long buttonTimer = 0;
long longPressTime = 250;

unsigned long previousMillis = 0;
unsigned long powerOff = 5000;
unsigned long currentMillis;

boolean buttonActive = false;
boolean longPressActive = false;

void setup() {
	Serial.begin(9600);
	logger_->setBluetoothCmd(false);
	logger_->setPrintDebugInfos(true);

	hardware->enableLedBacklight();
	senseoDruino->printSoftwareVersion();

	delay(2000);

	Serial.println("test_ Starting");
}

void loop() {

	// if no water in tank, we cannot begin!
	hardware->isWaterEmpty();

	if (hardware->getTemperationValue() <= hardware->getBoilerSensorValue()) {
		hardware->enableBlueLed();
		hardware->disableGreenLed();
	} else {
		hardware->disableBlueLed();
		hardware->enableGreenLed();
	}

	if (hardware->isButtonLeftPressed()) {
		logger_->debugInfo("button LEFT");
		Serial.println("button Left");
		if (menu->isMenuMode()) {
			menu->moveMenuCursorUp();
		} else {
			if (!hardware->getBoilerSensorValue()
					<= hardware->getTemperationValue()) {
				hardware->fillCup(45);
			}
//			hardware->disableBoiler();
		}
	}

	if (hardware->isButtonCenterPressed() == HIGH) {
		if (buttonActive == false) {
			buttonActive = true;
			buttonTimer = millis();
		}

		if ((millis() - buttonTimer > longPressTime)
				&& (longPressActive == false)) {
			longPressActive = true;

			logger_->debugInfo("Long Pressed");
			hardware->disableBlueLed();
			hardware->disableGreenLed();
			if (menu->isMenuMode()) {
				menu->setMenuMode(false);
				senseoDruino->printSoftwareVersion();
				if (menu->isAutoMode()) {
					display->lcdWriteLine("Auto-Mode: ON", 3, 2, false);
				}
				display->lcdWriteLine(menu->getSelectedSubMenuItem(), 4, 3,
				false);

			} else {
				menu->printMenue();
			}
		}

	} else {
		if (buttonActive == true) {
			if (longPressActive == true) {
				longPressActive = false;
			} else {
				logger_->debugInfo("SHORT PRESSED");
				if (menu->isMenuMode()) {
					mainMenuSelection();
				} else {
					hardware->heatBoiler();
				}
			}
			buttonActive = false;
		}
	}

// Button Right
	if (hardware->isButtonRightPressed()) {
		logger_->debugInfo("button Right");
		if (menu->isMenuMode()) {
			menu->moveMenuCursorDown();
		} else {
			if (!hardware->getBoilerSensorValue()
					<= hardware->getTemperationValue()) {
				hardware->fillCup(menu->getSelectedCupSize());
			}

		}
	}
	delay(100);

}

void serialEvent() {
	senseoDruino->clearInputString();
	while (Serial.available()) {
		char inChar = (char) Serial.read();
		senseoDruino->setInputString(inChar);
		if (inChar == '\n' || inChar == '\r') {
			senseoDruino->setStringComplete(true);
		}
		if (senseoDruino->getInputString().equals("POWERON")) {
			hardware->heatBoiler();
		} else if (senseoDruino->getInputString().equals("POWEROFF")) {
			hardware->disableBoiler();
		}
		display->lcdWriteLine(senseoDruino->getInputString(), 0, 3, true);
	}
	logger_->debugInfo(senseoDruino->getInputString());
}

void mainMenuSelection() {
	switch (menu->getSelectedMainMenuPosition()) {
	case 0:
		display->lcdWriteLine("Menu 0", 2, 1, true);
		temp();
		break;
	case 1:
//		display->lcdWriteLine("Menu 1", 2, 1, true);
		selectCupSize();
//		sendData("INFO&Hallo Cedric");
		break;
	case 2:
		senseoDruino->setAutoMode();
		break;
	case 3:
		lcdBacklight();
		break;
	case 4:
		display->lcdWriteLine("Menu 4", 2, 1, true);
		break;
	case 5:
		display->lcdWriteLine("Menu 5", 2, 1, true);
		break;
	}
	menu->printMenue();
}

// Menuoption 0
void temp() {
// ToDo: Add map method to map 0 - 520 to 0 - 20
	display->lcdClearDisplay();

	display->lcdWriteLine(senseo::ui::strings::TEMPERATION, 5, 1, false);
	display->lcdWriteLine(senseo::ui::strings::ARROW_LEFT, 0, 2, false);
	display->lcdWriteLine(senseo::ui::strings::ARROW_RIGHT,
			display->getMaxLcdCols() - 1, 2, false);

	while (!hardware->isButtonCenterPressed()) {
		delay(100);
		display->lcdWriteLine(senseo::ui::strings::TEMPERATION, 5, 1, true);
		display->lcdWriteLine(senseo::ui::strings::ARROW_LEFT, 0, 2, false);
		display->lcdWriteLine(senseo::ui::strings::ARROW_RIGHT, 9, 2, false);

		if (hardware->isButtonLeftPressed()) {
			hardware->setTemperationDown();
			display->lcdWriteLine(hardware->getTemperationValue(), 4, 2, false);
			logger_->debugInfo(hardware->getTemperationValue());
		} else if (hardware->isButtonRightPressed()) {
			hardware->setTemperationUp();
			display->lcdWriteLine(hardware->getTemperationValue(), 4, 2, false);
			logger_->debugInfo(hardware->getTemperationValue());
		}
		if (hardware->getTemperationValue() < hardware->MINTEMP_) {
			hardware->setTemperationValue(hardware->MINTEMP_);
		} else if (hardware->getTemperationValue() > hardware->MAXTEMP_) {
			hardware->setTemperationValue(hardware->MAXTEMP_);
		}
		display->lcdSetCursor(9, 2);
		int y = map(hardware->getTemperationValue(), 0, hardware->MAXTEMP_, 0,
				100);
		display->lcdWriteLine(y, 9, 2, false);
		logger_->debugInfo(y);
		display->lcdWriteLine(hardware->getTemperationValue() + "", 3, 2,
		false);
	}
	menu->printMenue();
}

void selectCupSize() {
	menu->setActiveMenu(menu->CUP_SIZES_MENU_);
	display->lcdClearDisplay();

	display->lcdSetCursor(2, 1);

	display->lcdWriteLine("Cup-Size", 5, 1, true);
	display->lcdWriteLine(menu->getSelectedSubMenuItem(), 0, 2, true);
	while (!hardware->isButtonCenterPressed()) {
		delay(100);
		if (hardware->isButtonRightPressed()) {
			display->lcdWriteLine(menu->getNextCupSizeItem(), 0, 2, true);
		} else if (hardware->isButtonLeftPressed()) {
			display->lcdWriteLine(menu->getPreviousCupSizeItem(), 0, 2, true);
		}
//		display->lcdWriteLine(menu->getSelectedSubMenuItem(), 4, 2, true);
		display->lcdWriteLine(senseo::ui::strings::ARROW_RIGHT,
				display->getMaxLcdCols() - 1, 2, false);
		display->lcdWriteLine(senseo::ui::strings::ARROW_LEFT, 0, 2, false);
	}
	menu->setActiveMenu(menu->MAIN_MENU_);
	menu->printMenue();

}

// Menu-Option 3
void SenseoDruino::setAutoMode() {
// ToDo: Add map method to map 0 - 520 to 0 - 20
	display->lcdClearDisplay();
	display->lcdWriteLine(senseo::ui::strings::AUTO_MODE, 5, 1, true);

	if (menu->isAutoMode()) {
		display->lcdWriteLine(senseo::ui::strings::ON, 9, 2, true);
	} else {
		display->lcdWriteLine(senseo::ui::strings::OFF, 9, 2, true);
	}
	display->lcdWriteLine(senseo::ui::strings::ARROW_RIGHT,
			display->getMaxLcdCols() - 1, 2, false);

	while (!hardware->isButtonCenterPressed()) {
		delay(100);
		if (hardware->isButtonLeftPressed()) {
			menu->setAutoMode(true);
		} else if (hardware->isButtonRightPressed()) {
			menu->setAutoMode(false);
		}
		display->lcdWriteLine(senseo::ui::strings::AUTO_MODE, 5, 1, true);
		if (menu->isAutoMode()) {
			display->lcdWriteLine(senseo::ui::strings::ON, 9, 2, false);
		} else {
			display->lcdWriteLine(senseo::ui::strings::OFF, 9, 2, false);
		}
		display->lcdWriteLine(senseo::ui::strings::ARROW_RIGHT,
				display->getMaxLcdCols() - 1, 2, false);
		display->lcdWriteLine(senseo::ui::strings::ARROW_LEFT, 0, 2, false);
	}
}

void SenseoDruino::printSoftwareVersion() {
	display->lcdClearDisplay();
	display->lcdWriteLine("SenseoDruino", 4, 1, true);
	if (menu->isAutoMode()) {
		display->lcdWriteLine("Auto-Mode: ON", 3, 2, true);
	}
	display->lcdWriteLine(menu->getSelectedSubMenuItem(), 4, 3, true);
}

void lcdBacklight() {
// ToDo: Add map method to map 0 - 520 to 0 - 20
	display->lcdClearDisplay();
//	display->lcdWriteLine("LCD Backlight", 5, 1, true);

	display->lcdWriteLine(senseo::ui::strings::ARROW_LEFT, 0, 2, false);
	display->lcdSetCursor(9, 2);

	display->lcdWriteLine(senseo::ui::strings::OFF, 9, 2, false); // ToDo: Add map method to map 0 - 520 to 0 - 20
	display->lcdClearDisplay();
	display->lcdWriteLine("BACKLIGHT", 5, 1, true);

	if (hardware->isDisplayOn()) {
		display->lcdWriteLine(senseo::ui::strings::ON, 9, 2, true);
	} else {
		display->lcdWriteLine(senseo::ui::strings::OFF, 9, 2, true);
	}
	display->lcdWriteLine(senseo::ui::strings::ARROW_RIGHT,
			display->getMaxLcdCols() - 1, 2, false);

	while (!hardware->isButtonCenterPressed()) {
		delay(100);
		if (hardware->isButtonLeftPressed()) {
			hardware->disableLedBacklight();
			display->lcdWriteLine(senseo::ui::strings::OFF, 9, 2, false);
		} else if (hardware->isButtonRightPressed()) {
			hardware->enableLedBacklight();
			display->lcdWriteLine(senseo::ui::strings::ON, 9, 2, false);
		}
		display->lcdWriteLine(senseo::ui::strings::ARROW_RIGHT,
				display->getMaxLcdCols() - 1, 2, false);
		display->lcdWriteLine(senseo::ui::strings::ARROW_LEFT, 0, 2, false);
	}

	display->lcdWriteLine(">", display->getMaxLcdCols() - 1, 2, false);
	menu->printMenue();
}

// *************** Bluetooth *****lcdWrite*********

void sendData(String data) {
	logger_->bluetooth(data);
}

bool isPowerOn() {
	hardware->heatBoiler();
	return senseoDruino->isPowerOnValue();
}

void setCupsEnableOnAndroid() {
	logger_->bluetooth("DONE&ENABLE_CUPS");
}

void setCupsDisableOnAndroid() {
	logger_->bluetooth("DONE&DISABLE_CUPS");
}

// *************** Bluetooth **************

/*
 int menuSenseoHardwareTest() {
 menuSenseoHardwareTestActive = true;
 lcd->clear();

 for (int i = 0; i < MAX_ROWS; i++) {
 lcd->setCursor(0, i);
 lcd->print(SenseoHardwareTestLines[DisplayFirstLine + i]);
 //    lcd->print();
 }
 lcd->setCursor(0, (CursorLine - DisplayFirstLine));
 //lcd->setCursor(0, CursorLine);
 lcd->write(">");
 }
 */
/*
 \40  !
 \41  "
 \42  #
 \43  $
 \44  %
 \45  &
 \50  (
 \51  )
 \52  *
 \53  Plus-Zeichen
 \54  Komma
 \55  Minus-Zeichen
 \56  Punkt
 \57  Schrägstrich
 \72  Doppelpunkt
 \73  Semikolon
 \74  <
 \75  Gleichheitszeichen
 \76  >
 \77  ?
 \100  @
 \134  eckige Klammer links
 \136  eckige Klammer rechts
 \137  accent circonflexe
 \138  Unterstrich
 \140  accent grave
 \173  geschweifte Klammer links
 \174  senkrechter Strich
 \175  geschweifte Klammer rechts
 \176  Pfeil nach rechts
 \177  Pfeil nach links
 \260  Minuszeichen
 \333  Kastenrahmen
 \337  hochgestellter Kastenrahmen(wie Potenz)
 \340  gr. alpha
 \341  ä
 \342  ß
 \343  klein epsilon
 \344  µ
 \350  Wurzelzeichen
 \351  hoch minus 1
 \353  hoch x
 \356  n mit Oberstrich ( spanisch )
 \357  ö
 \363  Zeichen unendlich
 \364  Ohm
 \365  ü
 \366  gr.Summe
 \367  pi ( klein )
 \371  u mit strich rechts unten
 \375  geteilt durch
 \377  alle Leuchtpunkte eingeschaltet
 */

