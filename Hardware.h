/*
 * Hardware.h
 *
 *  Created on: 20.01.2017
 *      Author: christianrichter
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

#include <Arduino.h>
#include <pins_arduino.h>
#include "Display.h"
#include "menu/Menu.h"
#include "libs/utils/Button.h"
#include "libs/utils/Strings.h"


class SenseoDruino;
class BluetoothHandler;

namespace senseo {
namespace electronic {

//#define P (17)
class Hardware {
public:
	Hardware(senseo::ui::Menu *menu, senseo::electronic::Display *display, logger::Logger *logger);

	int calculateProzentToHeat();
	int getBoilerSensorValue();

	void enableLedBacklight();
	void disableLedBacklight();

	void enableGreenLed();
	void disableGreenLed();
	void enableBlueLed();
	void disableBlueLed();
	void noWaterLed();

	void heatBoiler();
	void fillCup(int fillTime);
	bool isWaterHot();

	bool isButtonLeftPressed();
	bool isButtonCenterPressed();
	bool isButtonCenterLongPressed();
	bool isButtonRightPressed();

	int getBoilerSensor();
	void enableBoiler();
	void disableBoiler();

	void enablePump();
	void disablePump();

	bool isWaterEmpty();
	int getHallSensor();
	int getTemperationValue();
	void setTemperationValue(int temperationValue);
	void setTemperationDown();
	void setTemperationUp();

	bool isDisplayOn();

	void readButtons();

	void setRunning(bool running);
	void printSoftwareVersion();

	// Fixed Temp values
	int const MAXTEMP_ = 560.00;
	int const MINTEMP_ = 500.00;
	int const cupFillTime_ = 45;

private:
	int temperationValue_ = 500;		// the current Temperation value

	int const LEDBACKLIGHT_ = 10;		// Hardware Pin from the lcd background

	// Indicator LEDs
	int const GREENLED_ = 13;			// Hardware Pin from the Green LED
	int const BLUELED_ = 9;				// Hardware Pin from the blue LED

	// Senseo Devices Pins
	int const BOILER_ = 7;				// Hardware Pin from the boiler
	int const PUMP_ = 8;				// Hardware Pin from the pump

	// Button Pins
	int const BUTTONLEFT_ = A0;			// Hardware Pin from the leftButton
	int const BUTTONCENTER_ = A1;		// Hardware Pin from the middleButton
	int const BUTTONRIGHT_ = A2;		// Hardware Pin from the rightButton

	// Button values
	bool const PULLUP = true; 		 	// Internal Pullup Resistor
	bool const INVERT = false; 			// Invert the returning Value (like !pressed)
	bool const DEBOUNCE_MS = 300;		// Debouncing time for all buttons
	int const LONG_PRESS_TIME_= 1000;	// the holding time to enter in the menu mode

	// Sensors
	int const BOILERSENSOR_ = A3; 		// Hardware Pin from the boiler sensor
	int const HALLSENSOR_ = 4;			// Hardware Pin from the hall sensor

	bool stringComplete = false;		// set to true when the incomming command done
	bool running_ = false;				// set to true when the boiler is running


	senseo::ui::Menu *menu_;			// referenz to menu
	Display *display_;					// referenz to display

	Button *btnLeft_= new Button(BUTTONLEFT_, PULLUP, INVERT, DEBOUNCE_MS); 		// the referenz to left button
	Button *btnCenter_ = new Button(BUTTONCENTER_, PULLUP, INVERT, DEBOUNCE_MS);	// the referenz to center button
	Button *btnRight_= new Button(BUTTONRIGHT_, PULLUP, INVERT, DEBOUNCE_MS);		// the referenz to right button

	logger::Logger *logger_;

};
}

}
#endif /* HARDWARE_H_ */

